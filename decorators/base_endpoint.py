from typing import Callable
from app import app


def base_endpoint(fn: Callable) -> Callable:
    def wrapped(data: dict) -> dict:
        new_data = data

        new_data["footer_links"] = app.config["FOOTER_LINKS"]

        return fn(new_data)

    return wrapped
