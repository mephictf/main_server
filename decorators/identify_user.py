from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_jwt_extended.exceptions import JWTExtendedException
from flask import request
from db import User
from typing import Callable


def identify_user(fn) -> Callable:
    @jwt_required(optional=True, locations=["cookies"])
    def wrapped(data: dict) -> dict:

        new_data = data

        # try:
        identity = get_jwt_identity()

        if identity is not None:
            new_data["user"] = User.get_or_none(User.uuid == identity["id"])
        else:
            new_data["user"] = None

        # except JWTExtendedException:
        #     new_data["user"] = None

        return fn(new_data)

    return wrapped
