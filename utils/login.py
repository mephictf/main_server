from db import User
import requests


def login(token: str, host: str) -> User:
    info_json = requests.get(
        f"http://ulogin.ru/token.php?token={token}&host={host}"
    ).json()

    try:
        if info_json["network"] == "vkontakte":
            user = User.get(User.vk_user_id == int(info_json["uid"]))
        else:
            user = None
    except User.DoesNotExist:

        user = User.create(
            username=info_json["first_name"] + " " + info_json["last_name"],
            vk_user_id = int(info_json["uid"])
        )

    return user