from db import User
from flask_jwt_extended import create_refresh_token, create_access_token


def generate_jwt_tokens(user: User) -> dict:
    identity = {
        "id": user.uuid,
        "status": "user"
    }

    return {
        "access_token": create_access_token(identity, fresh=True),
        "refresh_token": create_refresh_token(identity)
    }
