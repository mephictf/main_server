from db.db import BaseModel

from peewee import (
    DeferredForeignKey,
    BooleanField,
    CharField
)


class TaskMatchTeam(BaseModel):
    task = DeferredForeignKey("Task")
    team = DeferredForeignKey("Team")

    flag = CharField(max_length=60)

    @property
    def solved(self):
        return self.task.flag == self.flag
