# -*- coding: utf-8 -*-
from peewee import Model, CharField, IntegerField
from playhouse.pool import PooledMySQLDatabase
from uuid import uuid4
from time import time
from peeweedbevolve import *  # noqa

from app import app

db = PooledMySQLDatabase(
    app.config['MYSQL']['db_name'],
    max_connections=32,
    stale_timeout=300,
    user=app.config['MYSQL']['user_name'],
    password=app.config['MYSQL']['password'],
    host=app.config['MYSQL']['host'],
    port=3306
)


def str_uuid():
    return str(uuid4())


class BaseModel(Model):
    uuid = CharField(max_length=64, default=str_uuid, unique=True, primary_key=True)
    timestamp = IntegerField(default=time)

    __fg_keys_aliased__ = {}
    __fields__ = []
    __fg_keys__ = []

    def get_dict(self, names: list[str] = [], exclude: list[str] = []):

        return {
            **{
                field: str(getattr(self, field))
                for field in self.__fields__ + ["uuid"]
                if hasattr(self, field) and getattr(self, field) is not None and
                (field in names or not names) and field not in exclude
            },
            **{
                field: getattr(field_obj, "get_dict")()
                for field in self.__fg_keys__
                if (field_obj := getattr(self, field)) and field_obj and
                hasattr(self, field) and (field in names or not names) and field not in exclude
            },
            **{
                val: {getattr(field_obj, "get_dict")()}
                for field, val in self.__fg_keys_aliased__.items()
                if (field_obj := getattr(self, field)) and field_obj and
                hasattr(self, field) and (field in names or not names) and field not in exclude
            }
        }

    def update_values(self, data: dict):
        for key, val in data.items():
            if hasattr(self, key):
                setattr(self, key, val)

    class Meta:
        database = db
