from __future__ import annotations

from db.db import BaseModel
from peewee import CharField


class Settings(BaseModel):
    name = CharField(index=True, max_length=30)
    value = CharField(max_length=255, default="")

    __fields__ = [
        "name", "value"
    ]

    @classmethod
    def get_setting(cls, name: str, default: str = "") -> Settings:
        setting, _ = cls.get_or_create(
            name=name,
            defaults={"value": default}
        )

        return setting

    @classmethod
    def update_setting(cls, name: str, value: str) -> None:
        setting: cls = cls.get_setting(name, value)

        if setting.value != value:
            setting.value = value
            setting.save()
