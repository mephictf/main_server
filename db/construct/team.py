from __future__ import annotations

from db.db import BaseModel
from db.match.task_team import TaskMatchTeam
from peewee import (
    CharField,
    ModelSelect,
    fn
)


class Team(BaseModel):
    name = CharField(max_length=40)

    @property
    def solved_tasks(self) -> ModelSelect:
        from db.construct.task import Task

        return self.tasks.where(TaskMatchTeam.flag == Task.flag)

    @property
    def tasks(self) -> ModelSelect:
        from db.construct.task import Task

        return Task.select().join(TaskMatchTeam) \
            .where(TaskMatchTeam.team == self) # \
            # .order_by(TaskMatchTeam.timestamp)

    @property
    def points(self) -> int:
        return sum([i.points for i in list(self.solved_tasks)])

    @staticmethod
    def get_top() -> list[Team]:
        from db.db import db

        with db.atomic():
            res = sorted(list(Team.select()), key=lambda team: team.points, reverse=True)

        return res
