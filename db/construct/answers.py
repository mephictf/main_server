from db.db import BaseModel
from peewee import (
    CharField,
    IntegerField,
    BooleanField,
    ForeignKeyField
)
from db.construct.user import User


class Answers(BaseModel):
	user = ForeignKeyField(User, backref="answers")
    
	task_name = CharField(max_length=100, default="")
	text_flag = CharField(max_length=100, default="")

	flag_is_true = BooleanField(default=False)
	points = IntegerField(default=0)
