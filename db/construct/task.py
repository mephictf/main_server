from db.db import BaseModel
from db.match.task_team import TaskMatchTeam
from peewee import (
    CharField,
    TextField,
    IntegerField,
    ModelSelect
)


class Task(BaseModel):
    name = CharField(max_length=100)
    flag = CharField(max_length=60)
    description = TextField()
    points = IntegerField()

    archive = TextField()

    @property
    def __attempts(self) -> ModelSelect:
        return TaskMatchTeam.select(TaskMatchTeam.uuid) \
            .where(TaskMatchTeam.task == self)
    
    @property
    def attempts(self) -> ModelSelect:
        return self.__attempts.count()

    @property
    def correct_attempts(self) -> ModelSelect:
        return self.__attempts.where(TaskMatchTeam.flag == self.flag).count()


    @property
    def teams_solved(self) -> ModelSelect:
        from db.construct.team import Team

        return Team.select().join(TaskMatchTeam).where(
                TaskMatchTeam.task == self,
                TaskMatchTeam.flag == self.flag
            ).order_by(TaskMatchTeam.timestamp)
