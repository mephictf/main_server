from db.db import BaseModel
from peewee import (
    CharField,
    IntegerField,
    BooleanField,
    ForeignKeyField
)
from db.construct.team import Team


class User(BaseModel):
    vk_user_id = IntegerField(index=True)
    is_admin = BooleanField(default=False)

    username = CharField(max_length=70)
    team = ForeignKeyField(Team, backref="users")
