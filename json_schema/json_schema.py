from typing import Union


class SchemaConstructor:
    # ------------------- main -------------------
    top_scheme = {
        "type": "object",
        "additionalProperties": False,
    }
    default_properties = {

    }
    uuid_schema = {"type": "string", "minLength": 36, "maLength": 36}
    name_schema = {"type": "string", "minLength": 2, "maxLength": 30}
    time_schema = {
        "type": "string",
        "pattern": r"^(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)((-(\d{2}):(\d{2}))?)$",
        "minLength": 8,
        "maxLength": 10
    }
    props_schema = {

    }


def get_schema(
        properties: list[Union[tuple[str, str], str]] = None,
        required: list[str] = None
) -> dict:
    res = {
        "properties": {**SchemaConstructor.default_properties},
        **SchemaConstructor.top_scheme,
        "required": []
    }

    properties = [] if not properties else properties
    required = [] if not required else required

    for p in properties:
        res["properties"][p] = SchemaConstructor.props_schema[p]

    for p in required:
        res["properties"][p] = SchemaConstructor.props_schema[p]
        res["required"].append(p)

    return res
