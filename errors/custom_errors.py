
class ErrException(Exception):
    def __init__(self, text: str, code: int, status_code: int = 200):
        self.error = text
        self.error_code = code
        self.status_code = status_code

    def keys(self):
        res = ["error", "error_code"]
        return res

    def __setitem__(self, key, value):
        if key not in ["error", "error_code", "status_code"]:
            raise KeyError

        setattr(self, key, value)

    def __getitem__(self, key):
        res = {
            "error": self.error,
            "error_code": self.error_code,
        }
        return res[key]


def get_error(err: ErrException) -> tuple[dict, int]:
    return dict(err), err.status_code
