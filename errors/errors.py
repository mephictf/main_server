from errors.custom_errors import ErrException

error_500 = ErrException("500 Internal Server Error", 500, 500)
error_404 = ErrException("404 Not Found", 404, 404)
error_405 = ErrException("405 Method Not Allowed", 405, 405)

error_type = ErrException("__custom__", 4)

error_data = ErrException("Error JSON data", 1003)
