# -*- coding: utf-8 -*-
import os
from dotenv import load_dotenv

import datetime

dot_env_path = os.path.join(os.path.dirname(__file__), "../.env")
if os.path.exists(dot_env_path):
    load_dotenv(dot_env_path)


class BaseConfig:
    __conf_type = os.environ.get("CONFIG_ENV")

    MYSQL: dict = {
        "db_name": os.environ.get("DB_NAME"),
        "user_name": os.environ.get("DB_USER_NAME"),
        "password": os.environ.get("DB_PASSWORD"),
        "host": os.environ.get("DB_HOST"),
        "port": os.environ.get("DB_PORT"),
    }

    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY")

    JWT_COOKIE_SECURE = True
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(
        seconds=int(os.environ.get("JWT_ACCESS_TOKEN_EXPIRES_TIMEDELTA"))
    )
    JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(
        days=int(os.environ.get("JWT_REFRESH_TOKEN_EXPIRES_TIMEDELTA"))
    )

    JWT_ACCESS_COOKIE_NAME = "access_token"
    JWT_REFRESH_COOKIE_NAME = "refresh_token"

    JWT_COOKIE_CSRF_PROTECT = True 
    JWT_ACCESS_CSRF_HEADER_NAME = "X-CSRF-TOKEN-ACCESS"
    JWT_REFRESH_CSRF_HEADER_NAME = "X-CSRF-TOKEN-REFRESH"

    ULOGIN_REDIRECT_TO = os.environ.get("ULOGIN_REDIRECT_TO")

    FOOTER_LINKS = {
		'kvant': '',
		'rrtu': '',
		'toup': '',
		'rostelecom': ''
	}


class DevelopmentConfig(BaseConfig):
    USE_CORS: bool = False

    DEBUG = True

    CORS: dict = {
        r"/private/*": {"origins": "*"},
        "/auth": {"origins": "*"},
        "/refresh": {"origins": "*"},
    }


class ProductionConfig(BaseConfig):
    DEBUG = False

    USE_CORS: bool = True

    CORS: dict = {
        r"/private/*": {"origins": "*"},
        "/auth": {"origins": "*"},
        "/refresh": {"origins": "*"},
    }
