function with_trailing_zero(s) {
    return s.length == 2 ? s : "0" + s;
}

(()=>{
    const daysLeft = document.getElementById('until-ctf-days')
    const hoursLeft = document.getElementById('until-ctf-hours')
    const minutesLeft = document.getElementById('until-ctf-minutes')
    const secondsLeft = document.getElementById('until-ctf-seconds')

    const updateTime = () => {
        const seconds = Number(secondsLeft?.textContent)
        const minutes = Number(minutesLeft?.textContent)
        const hours = Number(hoursLeft?.textContent)
        const days = Number(daysLeft?.textContent.replace(" days", ""))

        if(isNaN(seconds) | isNaN(minutes) | isNaN(hours) | isNaN(days)) {
            return
        }

        if(seconds > 0){
            secondsLeft.textContent = with_trailing_zero(`${seconds - 1}`)
        } else if( minutes > 0 ){
            secondsLeft.textContent = with_trailing_zero(`${59}`)
            minutesLeft.textContent = with_trailing_zero(`${minutes - 1}`)
        } else if( hours > 0 ){
            secondsLeft.textContent = with_trailing_zero(`${59}`)
            minutesLeft.textContent = with_trailing_zero(`${59}`)
            hoursLeft.textContent = with_trailing_zero(`${hours - 1}`)
        } else if( days > 0 ){
            secondsLeft.textContent = with_trailing_zero(`${59}`)
            minutesLeft.textContent = with_trailing_zero(`${59}`)
            hoursLeft.textContent = with_trailing_zero(`${23}`)
            daysLeft.textContent = with_trailing_zero(`${days - 1}`)
        }
    }

    setInterval(updateTime, 1000)
})()
