# -*- coding: utf-8 -*-
from flask import Flask, render_template, make_response
from flask_cors import CORS
from flask_bcrypt import Bcrypt

import os
from configs import config
from errors import error_404, error_500, error_405
from errors.custom_errors import ErrException, get_error
import traceback
# import re

app: Flask = Flask(__name__)

config_env: str = os.environ.get('CONFIG_ENV')
if hasattr(config, config_env):
    if config_env == 'DevelopmentConfig':
        os.environ["FLASK_ENV"] = 'development'

    app.config.from_object(getattr(config, config_env))
else:
    print('Use config class form config.py')
    exit()


if app.config['USE_CORS']:
    CORS(app, resources=app.config['CORS'])

bcrypt = Bcrypt(app)


@app.errorhandler(ErrException)
def custom_error_handler(error):
    return get_error(error)


@app.errorhandler(Exception)
def handler_500(error: Exception) -> tuple[dict, int]:
    print(traceback.format_exc())

    return dict(error_500), 500


@app.errorhandler(404)
def handler_404(error) -> tuple[dict, int]:
    return dict(error_404), 404


@app.errorhandler(405)
def handler_405(error) -> tuple[dict, int]:
    return dict(error_405), 405
