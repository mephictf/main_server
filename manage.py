# -*- coding: utf-8 -*-
import argparse
import sys
import os
import json
from pydantic import BaseModel, FilePath, ValidationError
from typing import Optional

usage_text: str = 'Use "manage.py migrate" to migrate database\n'
usage_text: str = 'Use "manage.py update_tasks" to syncronize info about tasks from tasks directory\n'

class FileTask(BaseModel):
    name: str
    points: int
    archive: Optional[FilePath] = ""
    flag: str
    description: str


class MyParser(argparse.ArgumentParser):
    def error(self, message) -> None:
        sys.exit()


my_parser: MyParser = MyParser()
my_parser.add_argument("pram")

def get_task_list() -> list[FileTask]:
    CONFIG_FILE = "tasks_config.json"
    tasks_dir = os.listdir(".")

    if CONFIG_FILE not in tasks_dir:
        print(f"No {CONFIG_FILE} file found")
        exit()

    with open(f"{CONFIG_FILE}", mode="r", encoding="utf-8") as f:    
        try:
            tasks_json = json.loads(f.read())
        except json.JSONDecodeError as e:
            print("Error while parsing `{}`:\n{}\n".format(CONFIG_FILE, e))
            exit()
    
    tasks = []
    names = []
    for i, task in enumerate(tasks_json):
        try:
            new_task = FileTask(**task)
            if new_task.name in names:
                print(f"\033[0;33mWARNING! Dublication task name `{new_task.name}` at place {i} -- Skipping.\033[0m")
                continue
            
            tasks.append(new_task)
            names.append(new_task.name)

        except ValidationError as e:
            print("\033[0;31mError while processing task `{}` ({}):\n{}\nSkipping.\n\033[0m".format(
                task.get("name", "<no name>"), i, e
            ))

    return tasks

args: argparse.Namespace = my_parser.parse_args()
if args.pram == "migrate":
    from db import *  # noqa

    db.evolve()  # noqa

elif args.pram == "update_tasks":
    from db import Task, db

    db_tasks_list: list[Task] = Task.select()
    file_tasks_list = get_task_list()

    if len(file_tasks_list) == 0:
        print("no tasks found!")
        exit()

    file_tasks = [i.name for i in file_tasks_list]
    db_tasks = [i.name for i in db_tasks_list]

    tasks_to_add: list[dict] = [
        task.dict() 
        for task in file_tasks_list
        if task.name not in db_tasks
    ]

    tasks_to_remove: list[Task] = [
        task
        for task in db_tasks_list
        if task.name not in file_tasks
    ]

    inp = input(
        "\nTask matching results:\n"
        f"   Tasks to insert: {len(tasks_to_add)} -- {[i['name'] for i in tasks_to_add]}\n"
        f"   Tasks to delete: {len(tasks_to_remove)} -- {[i.name for i in tasks_to_remove]}\n"
        f"   {len(file_tasks) - len(tasks_to_add)} more tasks would probably be updated\n\n"

        "Continue? [Y/n]: "
    )

    if inp != "Y":
        print("aborting.")
        exit()

    Task.insert_many(tasks_to_add).execute()
    Task.delete().where(Task.uuid.in_(tasks_to_remove)).execute()

    with db.atomic():
        for task in db_tasks_list:
            for file_task in file_tasks_list:
                if task.name == file_task.name:
                    task.update_values(file_task.dict())
                    task.save()

    print("ok")

else:
    print(usage_text)
