from flask import render_template, abort
from decorators.base_endpoint import base_endpoint
from decorators.identify_user import identify_user
from db import Settings, Team


@base_endpoint
@identify_user
def teams(data: dict):
    is_started = Settings.get_setting("is_ctf_started", "0")
    if is_started.value == "0":
        abort(404)
    
    return render_template(
        "teams.html",
        data=data,
        teams=list(Team.get_top()),
        title="Leaderboard"
    )
