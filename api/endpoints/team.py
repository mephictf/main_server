from flask import render_template, abort
from db.construct.team import Team
from db.construct.user import User
from decorators.base_endpoint import base_endpoint
from decorators.identify_user import identify_user


@base_endpoint
@identify_user
def team(data: dict):

    user: User = data["user"]

    if "uuid" not in data:
        abort(404)

    try:
        team: Team = Team.get_by_id(data["uuid"])
    except Team.DoesNotExist:
        abort(404)

    return render_template(
        "teams.html",
        user=user,
        team=team,
        data=data,
        title=f"Team: {team.name}"
    )
