from datetime import datetime, timedelta, timezone
from flask import render_template
from decorators.base_endpoint import base_endpoint
from decorators.identify_user import identify_user
from app import app

def with_trailing_zero(number: int) -> str:
    s = str(number)
    return s if len(s) == 2 else f"0{s}"

@base_endpoint
@identify_user
def index(data: dict):
    # print(data["user"])

    today = datetime.now(timezone(timedelta(hours=3)))
    start_at = datetime(2022, 10, 2, 22, 0, 0, tzinfo=timezone(timedelta(hours=3)))

    delta = start_at - today

    return render_template(
        "home.html",
        data=data,
        title="Присоединяйся к борьбе",
        ulogin_redirect_to=app.config["ULOGIN_REDIRECT_TO"],
        timer={
            "days": with_trailing_zero(delta.days),
            "hours": with_trailing_zero(delta.seconds // 60 // 60 % 24),
            "minutes": with_trailing_zero(delta.seconds // 60 % 60),
            "seconds": with_trailing_zero(delta.seconds % 60),
        }
    )
