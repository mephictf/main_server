from flask import render_template, abort
from decorators.base_endpoint import base_endpoint
from decorators.identify_user import identify_user
from db import Settings, Task


@base_endpoint
@identify_user
def tasks(data: dict):
    is_started = Settings.get_setting("is_ctf_started", "0")
    if is_started.value == "0":
        abort(404)

    return render_template(
        "tasks.html",
        data=data,
        tasks=Task.select(),
        title="Tasks"
    )
