import os
import importlib
from typing import Callable

from errors import error_404


class API:
    method_tree: dict = {}

    def __init__(self, api_name: str):
        root_path = os.path.dirname(os.path.abspath(__file__))

        self.views = {
            view: getattr(
                importlib.import_module(f"api.{api_name}.{view}"),
                view
            ) for view_file in os.listdir(f"{root_path}/{api_name}")
            if view_file.endswith(".py") and (view := view_file.replace(".py", ""))
        }

    def process(self, view: str, data: dict):
        try:
            view_function: Callable = self.views[view]
        except KeyError:
            raise error_404

        return view_function(data)
