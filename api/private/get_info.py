from decorators.json_validate import check_params
from decorators.identify_user import identify_user


@check_params()
@identify_user
def get_info(data: dict):

    print(data)

    return data["user"].get_dict()
